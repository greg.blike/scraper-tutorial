package tutorial

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.matchers.should.Matchers._
import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._
import net.ruippeixotog.scalascraper.dsl.DSL.Parse._
import net.ruippeixotog.scalascraper.model._
import net.ruippeixotog.scalascraper.scraper.{HtmlExtractor, PolyHtmlExtractor}

import scala.io.Source
import java.time.LocalDate

class ApplicationSpec extends AnyFlatSpec with Matchers {
  val htmlBody = Source.fromResource("test1.html").mkString("")
  val browser = JsoupBrowser()
  val doc = browser.parseString(htmlBody)
  val walmart = browser.parseString(Source.fromResource("walmart.html").mkString(""))

  case class Location(cityName: String, state: String)

  val locations: HtmlExtractor[Element, Iterable[Location]] = _
    .map({ text: Element =>
      val parts = text.text.split(",").map(_.strip())
      Location(parts(0), parts(1))
    })

  val location: HtmlExtractor[Element, Location] = _ >> locations.map(_.head)


  "The scraper when selecting the starred comment" should "say 'Third comment'" in {
    val answer: String = doc >> ???
    answer shouldBe "Third comment"
  }

  "The scraper when selecting the text of the comments" should  "select the text of the comments" in {
    val answer: Iterable[String] = doc >> ???
    answer should contain theSameElementsAs List(
      "First comment",
      "Second comment",
      "Third comment",
      "Fourth comment",
      "5th source"
    )
  }

  "The scraper when selecting a location" should  "extract the information to a location object" in {
    val answer: Location = walmart >> ???
    answer shouldBe Location("Bentonville", "Arkansas")
  }

  "The scrapper when selecting items that does not exist" should "be an empty list" in {
    val query = doc >> elements(".tertiary_content") >> locations
    query shouldBe ???
  }


  "The dates from the calendar in the example file" should "can extract 2023-01-29" in {
    import org.joda.time.LocalDate
    val answer = doc >> extractor(".date", texts, seq(asLocalDate("yyyy/MM/dd")))
    answer.toList(???) shouldBe LocalDate.parse("2023-01-29")
  }

  "Traversing tags" should "get the desired element" in {
   val answer = doc >> "body" >> "div#calendar" >> ".third" >> text
   answer shouldBe ???
  }
}
